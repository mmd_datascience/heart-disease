import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib as mpl
import matplotlib.pylab as pylab
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn import svm, tree, linear_model, neighbors, naive_bayes, ensemble, discriminant_analysis, gaussian_process
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn import feature_selection
from sklearn import model_selection
from sklearn import metrics
import xgboost as XGBClassifier
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder
import warnings
warnings.filterwarnings('ignore')

df = pd.read_csv('all_three_cities_processed.csv')

# replacing all ?'s with null values in order to see holes in data
df = df.replace('?', np.NaN)


# 2 columns of data are missing about half of the data. For simplicity, we will drop these columns for now.
df = df.drop(['Ca', 'Thal'], axis=1)

# every column that had a '?' is considered an object column, and we need to change them into int

df['Trestbps'] = df['Trestbps'].astype(str).astype(float)
df['Chol'] = df['Chol'].astype(str).astype(float)
df['Thalach'] = df['Thalach'].astype(str).astype(float)
df['Oldpeak'] = df['Oldpeak'].astype(str).astype(float)
df['Fbs'] = df['Fbs'].astype(str).astype(float)
df['Restecg'] = df['Restecg'].astype(str).astype(float)
df['Exang'] = df['Exang'].astype(str).astype(float)
df['Slope'] = df['Slope'].astype(str).astype(float)


# multiple columns are missing data, this will be where we will spend a lot of time figuring out the best way to
# fill in the missing data. For now, we will use the mean for 4 columns, and the mode for 4 columns.

df['Trestbps'] = df['Trestbps'].fillna(df['Trestbps'].mean())
df['Chol'] = df['Chol'].fillna(df['Chol'].mean())
df['Thalach'] = df['Thalach'].fillna(df['Thalach'].mean())
df['Oldpeak'] = df['Oldpeak'].fillna(df['Oldpeak'].mean())

# when we see what mode returns, it's a series with a single row, so when you pass this to fillna it
# only fills the first row, so what you want is to get the scalar value by indexing into the Series

df['Fbs'] = df['Fbs'].fillna(df['Fbs'].mode()[0])
df['Restecg'] = df['Restecg'].fillna(df['Restecg'].mode()[0])
df['Exang'] = df['Exang'].fillna(df['Exang'].mode()[0])
df['Slope'] = df['Slope'].fillna(df['Slope'].mode()[0])

# Machine Learning Algorithm Selection and Initialization
# This is a classification question, so we are running classifiers instead of regression models.
MLA = [
    # Ensemble Methods
    ensemble.AdaBoostClassifier(),
    ensemble.BaggingClassifier(),
    ensemble.ExtraTreesClassifier(),
    ensemble.GradientBoostingClassifier(),
    ensemble.RandomForestClassifier(),

    # Gaussian Processes
    gaussian_process.GaussianProcessClassifier(),

    # GLM
    linear_model.LogisticRegressionCV(),
    linear_model.PassiveAggressiveClassifier(),
    linear_model.RidgeClassifierCV(),
    linear_model.SGDClassifier(),
    linear_model.Perceptron(),

    # Navies Bayes
    naive_bayes.BernoulliNB(),
    naive_bayes.GaussianNB(),

    # Nearest Neighbor
    neighbors.KNeighborsClassifier(),

    # SVM
    #svm.SVC(probability=True),
    #svm.NuSVC(probability=True),
    #svm.LinearSVC(),

    # Trees
    tree.DecisionTreeClassifier(),
    tree.ExtraTreeClassifier(),

    # Discriminant Analysis
    discriminant_analysis.LinearDiscriminantAnalysis(),
    discriminant_analysis.QuadraticDiscriminantAnalysis(),

    # xgboost
    XGBClassifier.XGBClassifier()
]

#splitting data into training data and test data
cv_split = model_selection.ShuffleSplit(n_splits=10, test_size=.3, train_size=.7, random_state=0)

#create table to compare MLA metrics
MLA_columns = ['MLA Name', 'MLA Parameters', 'MLA Train Accuracy Mean', 'MLA Test Accuracy Mean',
               'MLA Test Accuracy 3*STD', 'MLA Time']
MLA_compare = pd.DataFrame(columns=MLA_columns)

#create table to compare MLA predictions
df_x_bin = ['Age', 'Sex', 'CP', 'Trestbps', 'Chol', 'Fbs', 'Restecg', 'Thalach', 'Exang',
                    'Oldpeak', 'Slope', 'City']
MLA_predict = df['Num']


#indexing through MLA and saving the performance to a table
row_index = 0
for alg in MLA:
    # set name and parameters
    MLA_name = alg.__class__.__name__
    MLA_compare.loc[row_index, 'MLA Name'] = MLA_name
    MLA_compare.loc[row_index, 'MLA Parameters'] = str(alg.get_params())

    # score model with cross validation: http://scikit-learn.org/stable/modules/generated/sklearn.model_selection.cross_validate.html#sklearn.model_selection.cross_validate
    cv_results = model_selection.cross_validate(alg, df[df_x_bin], df['Num'], cv=cv_split, return_train_score = True)

    MLA_compare.loc[row_index, 'MLA Time'] = cv_results['fit_time'].mean()
    MLA_compare.loc[row_index, 'MLA Train Accuracy Mean'] = cv_results['train_score'].mean()
    MLA_compare.loc[row_index, 'MLA Test Accuracy Mean'] = cv_results['test_score'].mean()
    # if this is a non-bias random sample, then +/-3 standard deviations (std) from the mean, should statistically capture 99.7% of the subsets
    MLA_compare.loc[row_index, 'MLA Test Accuracy 3*STD'] = cv_results[
                                                                'test_score'].std() * 3  # let's know the worst that can happen!

    # save MLA predictions
    alg.fit(df[df_x_bin], df['Num'])
    MLA_predict[MLA_name] = alg.predict(df[df_x_bin])

    row_index += 1

#print and sort table
MLA_compare.sort_values(by = ['MLA Test Accuracy Mean'], ascending = False, inplace = True)


#barplot
sns.barplot(x='MLA Test Accuracy Mean', y = 'MLA Name', data = MLA_compare, color = 'm')

#make it look fancy using pyplot
plt.title('Machine Learning Algorithm Accuracy Score \n')
plt.xlabel('Accuracy Score (%)')
plt.ylabel('Algorithm')

plt.show()


